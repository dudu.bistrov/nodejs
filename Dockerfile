FROM centos:centos7
RUN curl -sL https://rpm.nodesource.com/setup_8.x | bash - && \
    yum install java-1.8.0-openjdk nodejs gcc-c++ make -y && \
    curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo && \
    rpm --import https://dl.yarnpkg.com/rpm/pubkey.gpg && \
    yum install yarn -y
#sudo docker build -t test/test-nodejs:3 .
#sudo docker run --name nodejs -idt test/test-nodejs:3
CMD ["/bin/bash"]